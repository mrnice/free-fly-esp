Free fly ESP
===========


This project aims to use the famous ESP to connect cheep flying sensors to Android
ebook readers without the need to modify the reader hardware. As most ebook readers
have Wifi chips build in.

The sensors used in this project are:

* GY-86 module (~15 Euro)
  * MS5611-01BA03 barometic pressure sensor (with ~10cm resolution)
  * MPU-6050 9-axis gyrometer
  * HMC5883L 3-axis digital compass
* GY-GPS6MV2
  * myBlox NEO-6M-0-001 over UART

Later I will also add a PWM beeper and a OLED display. To make it a fully working backup variometer.
For easy powering I use a USB backup charger. The ESP I am using is a nodemcu clone.

I will try to keep the UART of the ESP free so that we will be able to use this as an USB gadget device.

A wired connection should have less latency. But because the refresh rate of an ebook reader is bad anyways
it should not harm to use the Wifi link if your ebook reader has no USB gadget or you prefere to have it wireless.

On the ebook reader side there will be an Android sensor driver for each of the sensors. Therefore all Android
Apps which make use of any of the sensors will work without modification. My target is to get XC Soar and XC Track
running. And later write a free XC Track replacement which is optimized for the ebook readers.

Any help is of course verry welcome

Happy hacking :)